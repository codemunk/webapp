# Gitter account

A Gitter account is associated with GitLab/GitHub/Twitter and matches whatever info you have on that platform.

If you updated some info on GitLab/GitHub/Twitter, sign out of Gitter and sign back in to have it updated.


## Can I merge/connect my accounts?

There isn't a way to merge accounts.


## Can I change my username?

You can't change your username. Your username matches whatever OAuth provider you signed in with.

If you changed your username on GitHub, sign out of Gitter and sign back in again to update it.


### How do I remove the  `_gitlab`/`_twitter` suffix from my username

See above, you can't change your username.

We add the `_gitlab`/`_twitter` suffix to avoid name collisions with GitHub.
If you don't want the suffix added, sign in with GitHub.

You can track [#1851](https://gitlab.com/gitlab-org/gitter/webapp/issues/1851)
for the ability to customize your username in the future and remove the suffix.

## How do I update my avatar?

Sign out of Gitter and sign back in to update your avatar (or any other info).


## How do I delete my account?

You can delete your account by using the profile menu dropdown in the top-right -> **Delete Account**

![](https://i.imgur.com/j3Gowl7.png)

We can't recover your data after deletion but you can re-create your account at any time by signing back in.
